import argparse
import os
import re
from sys import platform

parser = argparse.ArgumentParser(description="Gitignore files.")
parser.add_argument('--project_dir', type=str, help='project path')
args = parser.parse_args()
gitignore_files = []
git_file = open(os.path.join(args.project_dir, '.gitignore'), 'r')

for line in git_file:
    for el in line:
        if (el=='/') and (platform=="win32"):
            el=='\\'
    line = line.strip('')
    gitignore_files.append(line)
    print(line)
path_f = []

for dir, dirs, files in os.walk(args.project_dir):
    for file in files:
        path = os.path.join(dir, file)
        path_f.append(path)

print("Ignored files:")
for gitign_el in gitignore_files:
    if (gitign_el[0] == '*'):
        new_name = gitign_el[1:]
        for path_el in path_f:
            if (path_el.endswith(new_name)):
                print(f"{path_el} ignored by expression *{new_name}")
    else:
        for path_el in path_f:
            if gitign_el in path_el:
                print(f"{path_el} ignored by expression {gitign_el}")
import numpy as np
import pandas as pd
from flask import Flask, request, render_template

app = Flask(__name__)
app.config['JSON_AS_ASCII'] = False

def read_table(page_name: str) -> object:
    read_table.__df = pd.read_csv(page_name+'.csv',engine="python",encoding='utf-8')
    return read_table.__df
hw_01 =read_table('hw-01')
hw_02=read_table('hw-02')
def open_table(hw_name:str)-> object:
    if hw_name=='hw-01':
        df=hw_01
    elif hw_name=='hw-02':
        df=hw_02  
    return df
    

@app.route('/names')
def names():
    df= hw_01
    return {
        'names': [name for name in df['ФИ']],
    }


@app.route('/<hw_name>/mean_score')
def mean_score_by_hw(hw_name: str)-> str:
    df=open_table(hw_name)       
    return str(np.nanmean(df['Количество баллов']))


@app.route('/<hw_name>/<group_id>/mean_score')
def mean_score(group_id: int,hw_name: str)-> str:
    df=open_table(hw_name)
    return str(np.nanmean(df.loc[df['Группа']==group_id,'Количество баллов']))


@app.route('/mean_score')
def mean_score_get_values():
    group_id = request.args.get('group')
    hw_name = request.args.get('hw')
    df=open_table(hw_name)
    return str(np.nanmean(df.loc[df['Группа']==int(group_id),'Количество баллов']))


def marks(points: float)-> str:
    if points > 50:
        mark='5'
    elif points > 30:
        mark='4'
    elif points > 0:
        mark='3'
    else:
        mark='2'
    return mark


@app.route('/mark')
def mark_student():
    student_id = int(request.args.get('student_id'))
    if student_id==20144 or student_id==20137:
        return mark_group(student_id)
    else:
        df1=hw_01
        df2=hw_02
        sum =list(df1[df1['№']==student_id]['Количество баллов'])[0]+list(df2[df2['№']==student_id]['Количество баллов'])[0]
        return marks(sum)


def mark_group(group_id: int) -> str:
    df1 = hw_01
    df2 = hw_02
    sum =np.nanmean(df1.loc[df1['Группа']==group_id,'Количество баллов'])+np.nanmean(df2.loc[df2['Группа']==group_id,'Количество баллов'])
    return marks(sum)


@app.route('/course_table')
def render_all():
    group_id= request.args.get('group_id')
    hw_name= request.args.get('hw_name')
    if hw_name ==None:
        return {'error': 'vse upalo', 'message': 'potomu chto plohoy zapros'}, 400
    elif group_id ==None:
        df = open_table(hw_name)
        return render_template('table.html', columns=df.columns, rows=df.values)
    else:
        df = open_table(hw_name)
        group_df=df[df['Группа']==int(group_id)]
        return render_template('table.html', columns=group_df.columns, rows=group_df.values)


if __name__ == '__main__':
    app.run('127.0.0.1', port=1337)

import argparse
import os
import time

parser = argparse.ArgumentParser(description="delete files.")
parser.add_argument('--trash_folder_path', type=str, required=True, help='project path')
parser.add_argument('--age_thr', type=int, required=True, help='age of files')
args = parser.parse_args()
path_f = []
del_f = open("clean_trash.log", 'w')
for dir, dirs, files in os.walk(args.trash_folder_path):
    for file in files:
        path = os.path.join(dir, file)
        new_time = time.time() - os.path.getmtime(path)
        time_in_min = new_time / 60
        if time_in_min > args.age_thr:
            del_f.write(path + '\n')
            os.remove(path)
    time.sleep(1)
    for dir_del in dirs:
        path_dir = os.path.join(dir, dir_del)
        if len(os.listdir(path_dir)) == 0:
            os.rmdir(path_dir)
del_f.close()
